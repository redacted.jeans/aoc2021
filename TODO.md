# TODOs

The goals of this project are: first, to learn Elixir; second, to get all the stars; and third, to make my solutions fast (defined as less than 1s runtime for both parts).

## Speed
- day 23 (amphipods/A*) takes around 10 seconds; making this faster might involve a better heuristic? (making this day faster might also help with day 15's A*)
- day 15 (chiton cave/A*) takes 2.2-2.5 seconds; can I make it a bit faster (sub-2s runtime?)
- day 19... sucks (7 minutes/part, 15 minutes for both; if I ran the alg once for both parts instead of once per part that would be better but still bad)
- day 20 (image enhancement/cellular automata) takes 3-5 seconds; one approach to making it faster (if algorithmic improvements are out of reach) would be to parallelize (for instance, see [Flow](https://hexdocs.pm/flow/Flow.html))
- 05 and 21 are in the 1.3s-1.5s range, which is acceptable but has room for improvement
- 12, 17, 22 are all ~1s (±0.1s), which is fine but if I'm bored I could take a look (shoot for consistently sub-1s)