defmodule AoC2021.MixProject do
  use Mix.Project

  def project do
    [
      app: :aoc2021,
      version: "0.1.0",
      elixir: "~> 1.14-dev",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_paths: ["lib"],
      test_pattern: "test.exs",
      test_coverage: [
        ignore_modules: [Mix.Tasks.Day]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:heap, "~> 2.0.2"}
    ]
  end
end
