defmodule AoC2021.Day02 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(&parse(&1))
  end

  defp parse(line) do
    [cmd, n] = String.split(line, " ", trim: true)
    [String.to_atom(cmd), String.to_integer(n)]
  end

  # Part 1
  # coords are tracked in a list [x, y]
  def part1(cmds) do
    cmds
    |> Enum.reduce([0, 0], fn [cmd, n], [x, y] ->
      case cmd do
        :forward -> [x + n, y]
        :down -> [x, y + n]
        :up -> [x, y - n]
      end
    end)
    |> Kernel.then(fn [x, y] -> x * y end)
  end

  # Part 2
  # coords are tracked in a list [x, y, aim]
  def part2(cmds) do
    cmds
    |> Enum.reduce([0, 0, 0], fn [cmd, n], [x, y, a] ->
      case cmd do
        :forward -> [x + n, y + a * n, a]
        :down -> [x, y, a + n]
        :up -> [x, y, a - n]
      end
    end)
    |> Kernel.then(fn [x, y, _] -> x * y end)
  end
end
