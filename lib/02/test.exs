defmodule AoC2021.Test02 do
  use ExUnit.Case, async: true

  alias AoC2021.Day02

  @data [
    [:forward, 5],
    [:down, 5],
    [:forward, 8],
    [:up, 3],
    [:down, 8],
    [:forward, 2]
  ]

  test "setup" do
    assert Day02.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day02.part1(@data) === 150
  end

  test "part2" do
    assert Day02.part2(@data) === 900
  end
end
