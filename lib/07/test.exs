defmodule AoC2021.Test07 do
  use ExUnit.Case, async: true

  alias AoC2021.Day07

  @data [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]

  test "setup" do
    assert Day07.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day07.part1(@data) === 37
  end

  test "part2" do
    assert Day07.part2(@data) === 168
  end
end
