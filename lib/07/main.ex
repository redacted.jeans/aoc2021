defmodule AoC2021.Day07 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split([",", "\n"], trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  # Part 1
  def part1(init) do
    # get range of positions and group crabs by position
    {min, max} = Enum.min_max(init)
    fqs = Enum.frequencies(init)

    # calculate cost for each destination position
    Enum.map(min..max, fn dest ->
      # add up the cost for each cohort of crabs to move
      Enum.reduce(fqs, 0, fn {src, num}, cost ->
        cost + abs(dest - src) * num
      end)
    end)
    # pick the lowest cost
    |> Enum.min()
  end

  # Part 2
  def part2(init) do
    # get range of positions and group crabs by position
    {min, max} = Enum.min_max(init)
    fqs = Enum.frequencies(init)

    # calculate cost for each destination position
    Enum.map(min..max, fn dest ->
      # add up the cost for each cohort of crabs to move
      Enum.reduce(fqs, 0, fn {src, num}, cost ->
        dist = abs(dest - src)
        cost + dist * (dist + 1) / 2 * num
      end)
    end)
    # pick the lowest cost
    |> Enum.min()
    |> round()
  end
end
