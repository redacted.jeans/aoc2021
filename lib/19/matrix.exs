defmodule AoC2021.Matrix do
  def permutations() do
    # generate all normalized(?) 3x3 matrices
    for xx <- -1..1, xy <- -1..1, xz <- -1..1,
        yx <- -1..1, yy <- -1..1, yz <- -1..1,
        zx <- -1..1, zy <- -1..1, zz <- -1..1,
        abs(xx) + abs(xy) + abs(xz) === 1,  # only one non-zero in x row
        abs(xx) + abs(yx) + abs(zx) === 1,  # only one non-zero in x col
        abs(yx) + abs(yy) + abs(yz) === 1,  # only one non-zero in y row
        abs(xy) + abs(yy) + abs(zy) === 1,  # only one non-zero in y col
        abs(zx) + abs(zy) + abs(zz) === 1,  # only one non-zero in z row
        abs(xz) + abs(yz) + abs(zz) === 1,  # only one non-zero in z col
        true do
      [
        [xx, xy, xz],
        [yx, yy, yz],
        [zx, zy, zz]
      ]
    end
  end

  # for 2x2 matrix:
  def determinant([[a, b], [c, d]]), do: (a * d) - (b * c)
  # for 3x3 matrix:
  def determinant([[a, b, c], [d, e, f], [g, h, i]]) do
    (determinant([[e,f],[h,i]]) * a) - (determinant([[d,f],[g,i]]) * b) + (determinant([[d,e],[g,h]]) * c)
  end
end

alias AoC2021.Matrix

Matrix.permutations()
|> Enum.filter(&match?(1, Matrix.determinant(&1)))
|> IO.inspect()
|> Enum.count()
|> IO.inspect(label: "number of rotation matrices") # should be 24
