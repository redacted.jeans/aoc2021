defmodule AoC2021.Day19 do
  # all valid (multiples of 90deg) rotation matrices
  # these matrices were computed by matrix.exs
  @rms [
    [[-1, 0, 0], [0, -1, 0], [0, 0, 1]],
    [[-1, 0, 0], [0, 0, -1], [0, -1, 0]],
    [[-1, 0, 0], [0, 0, 1], [0, 1, 0]],
    [[-1, 0, 0], [0, 1, 0], [0, 0, -1]],
    [[0, -1, 0], [-1, 0, 0], [0, 0, -1]],
    [[0, -1, 0], [0, 0, -1], [1, 0, 0]],
    [[0, -1, 0], [0, 0, 1], [-1, 0, 0]],
    [[0, -1, 0], [1, 0, 0], [0, 0, 1]],
    [[0, 0, -1], [-1, 0, 0], [0, 1, 0]],
    [[0, 0, -1], [0, -1, 0], [-1, 0, 0]],
    [[0, 0, -1], [0, 1, 0], [1, 0, 0]],
    [[0, 0, -1], [1, 0, 0], [0, -1, 0]],
    [[0, 0, 1], [-1, 0, 0], [0, -1, 0]],
    [[0, 0, 1], [0, -1, 0], [1, 0, 0]],
    [[0, 0, 1], [0, 1, 0], [-1, 0, 0]],
    [[0, 0, 1], [1, 0, 0], [0, 1, 0]],
    [[0, 1, 0], [-1, 0, 0], [0, 0, 1]],
    [[0, 1, 0], [0, 0, -1], [-1, 0, 0]],
    [[0, 1, 0], [0, 0, 1], [1, 0, 0]],
    [[0, 1, 0], [1, 0, 0], [0, 0, -1]],
    [[1, 0, 0], [0, -1, 0], [0, 0, -1]],
    [[1, 0, 0], [0, 0, -1], [0, 1, 0]],
    [[1, 0, 0], [0, 0, 1], [0, -1, 0]],
    [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
  ]

  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n\n", trim: true)
    |> Enum.map(fn coords ->
      [_ | coords] = String.split(coords, ["\n", ","], trim: true)

      Enum.map(coords, &String.to_integer/1)
      |> Enum.chunk_every(3)
      |> MapSet.new()
    end)
  end

  # subtract vector v2 from vector v1
  defp vsub(v1, v2) when length(v1) !== length(v2), do: raise("mismatched vector sizes")
  defp vsub(v1, v2), do: Enum.zip(v1, v2) |> Enum.map(fn {n1, n2} -> n1 - n2 end)

  # add vectors v1 and v2
  defp vadd(v1, v2) when length(v1) !== length(v2), do: raise("mismatched vector sizes")
  defp vadd(v1, v2), do: Enum.zip(v1, v2) |> Enum.map(&Tuple.sum/1)

  # multiply vector v by matrix m
  defp product(m, v) when length(m) !== length(v), do: raise("mismatched vector and matrix sizes")
  defp product(m, v) do
    for row <- m, do: Enum.zip(v, row) |> Enum.map(&Tuple.product/1) |> Enum.sum()
  end

  # compares two sets of coordinates and returns the max possible number of matches between them,
  # as well as the offset relative to ref that produces that number
  defp compare(coords, refs) do
    for ref <- refs, coord <- coords do
      offset = vsub(ref, coord)
      matches = Enum.map(coords, &vadd(&1, offset)) |> MapSet.new() |> MapSet.intersection(refs) |> MapSet.size()
      {matches, offset}
    end
    |> Enum.max_by(fn {m, _} -> m end)
  end

  # recurse through the rotation matrices, checking whether we can situate rotated coords relative to ref
  # returns {offset, rotated coords} on success (first rotation matrix with 12+ matches),
  #         {nil, coords} on failure (no rotation matrix worked)
  defp rotate(ref, coords), do: rotate(ref, coords, @rms)
  defp rotate(_, coords, []), do: {nil, coords}
  defp rotate(ref, coords, [rotation | rest]) do
    # rotate coords
    rotated = Enum.map(coords, &product(rotation, &1))
    # see if rotated coords overlap: if so return them with offset; otherwise recurse
    case compare(rotated, ref) do
      {m, _} when m < 12 -> rotate(ref, coords, rest)
      {_, offset} -> {offset, MapSet.new(rotated)}
    end
  end
  
  # try matching a set of coordinates against a list of reference sets
  # returns {offset (relative to scanner 0), rotated coords} if successful,
  #         coords otherwise
  defp relative([], coords), do: coords
  defp relative([{base, ref} | rest], coords) do
    case rotate(ref, coords) do
      {nil, _} -> relative(rest, coords)
      {off, rotated} -> {vadd(base, off), rotated}
    end
  end

  # find the offsets for all sets of coordinates relative to the first set
  # returns a list of {offset, rotated coords}
  defp offset([ref | others]), do: offset([{[0, 0, 0], ref}], others)
  defp offset(known, []), do: known
  defp offset(known, unknown) do
    # try to match each unknown with any in the reference set (known)
    {found, unknown} =
      Enum.map(unknown, &relative(known, &1))
      |> Enum.split_with(&is_tuple/1)

    offset(found ++ known, unknown)
  end
  
  # Part 1
  def part1(scanners), do: offset(scanners) |> beacons() |> MapSet.size()

  # gets the set of all beacon coordinates relative to scanner 0
  defp beacons(scanners) do
    Enum.reduce(scanners, MapSet.new(), fn {offset, coords}, set ->
      Enum.map(coords, &vadd(offset, &1))
      |> MapSet.new()
      |> MapSet.union(set)
    end)
  end

  # Part 2
  def part2(scanners) do
    coords = offset(scanners)

    for {c1, _} <- coords, {c2, _} <- coords, c1 !== c2 do
      manhattan(c1, c2)
    end
    |> Enum.max()
  end

  defp manhattan([x0, y0, z0], [x1, y1, z1]), do: abs(x1 - x0) + abs(y1 - y0) + abs(z1 - z0)
end
