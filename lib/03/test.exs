defmodule AoC2021.Test03 do
  use ExUnit.Case, async: true

  alias AoC2021.Day03

  @data [
    [0, 0, 1, 0, 0],
    [1, 1, 1, 1, 0],
    [1, 0, 1, 1, 0],
    [1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1],
    [0, 1, 1, 1, 1],
    [0, 0, 1, 1, 1],
    [1, 1, 1, 0, 0],
    [1, 0, 0, 0, 0],
    [1, 1, 0, 0, 1],
    [0, 0, 0, 1, 0],
    [0, 1, 0, 1, 0]
  ]

  test "setup" do
    assert Day03.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day03.part1(@data) === 198
  end

  test "part2" do
    assert Day03.part2(@data) === 230
  end
end
