defmodule AoC2021.Day03 do
  use Bitwise

  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(fn row ->
      Enum.map(String.codepoints(row), &String.to_integer/1)
    end)
  end

  # Part 1
  def part1(bin) do
    bin
    |> Enum.zip_reduce({0, 0}, &get_rates/2)
    |> Tuple.product()
  end

  # returns a tuple containing {gamma, epsilon}
  defp get_rates(row, {g, e}) do
    case Enum.frequencies(row) do
      %{0 => zs, 1 => os} when zs > os ->
        {g <<< 1, e <<< 1 ||| 1}

      _ ->
        {g <<< 1 ||| 1, e <<< 1}
    end
  end

  # Part 2
  def part2(bin) do
    o2 = get_rating(bin, 0, :o2)
    co2 = get_rating(bin, 0, :co2)

    o2 * co2
  end

  # base case: if only one item left in the list,
  # convert to decimal and return
  defp get_rating([val], _, _) do
    Enum.reduce(val, fn d, b -> b <<< 1 ||| d end)
  end

  # recursive case: take the ith digit of every number, calculate the frequencies of 0s and 1s,
  # and figure out the criteria;
  # then filter out numbers based on that criteria and recurse on the remaining numbers
  defp get_rating(bin, i, type) do
    crit =
      Stream.map(bin, &Enum.at(&1, i))
      |> Enum.frequencies()
      |> then(&get_crit(&1, type))

    bin
    |> Enum.filter(fn num -> Enum.at(num, i) === crit end)
    |> get_rating(i + 1, type)
  end

  # oxygen rating criteria: return most common digit, 1 if tie
  defp get_crit(%{0 => zs, 1 => os}, :o2) when zs <= os, do: 1
  defp get_crit(%{0 => zs, 1 => os}, :o2) when zs > os, do: 0
  defp get_crit(%{0 => _}, :o2), do: 0
  defp get_crit(%{1 => _}, :o2), do: 1
  # co2 rating criteria: return least common digit, 0 if tie
  defp get_crit(%{0 => zs, 1 => os}, :co2) when zs <= os, do: 0
  defp get_crit(%{0 => zs, 1 => os}, :co2) when zs > os, do: 1
  defp get_crit(%{0 => _}, :co2), do: 1
  defp get_crit(%{1 => _}, :co2), do: 0
end
