defmodule AoC2021.Test13 do
  use ExUnit.Case, async: true

  alias AoC2021.Day13

  # NOTE: test data was sorted so it doesn't match the website exactly
  @dots [
    {3, 0}, {6, 0}, {9, 0},
    {4, 1},
    {0, 3},
    {3, 4}, {8, 4}, {10, 4},
    {1, 10}, {6, 10}, {8, 10}, {9, 10},
    {4, 11},
    {6, 12}, {10, 12},
    {0, 13},
    {0, 14}, {2, 14}
  ]
  @folds [{:up, 7}, {:left, 5}]

  test "setup" do
    assert Day13.setup("#{__DIR__}/data/test.txt") === {@dots, @folds}
  end

  test "part1" do
    assert Day13.part1({@dots, @folds}) === 17
  end

  test "part2" do
    assert Day13.part2({@dots, @folds}) === "\n██████████\n██      ██\n██      ██\n██      ██\n██████████"
  end
end
