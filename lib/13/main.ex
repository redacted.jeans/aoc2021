defmodule AoC2021.Day13 do
  # Setup, etc.
  def setup(file) do
    [dots, folds] =
      File.read!(file)
      |> String.split("\n\n", trim: true)

    dots =
      String.split(dots, ["\n", ","], trim: true)
      |> Enum.chunk_every(2)
      |> Enum.map(fn [x, y] -> {String.to_integer(x), String.to_integer(y)} end)

    folds =
      String.split(folds, "\n", trim: true)
      |> Enum.map(fn
        "fold along x=" <> num -> {:left, String.to_integer(num)}
        "fold along y=" <> num -> {:up, String.to_integer(num)}
      end)

    {dots, folds}
  end

  # I'm assuming a fold can't go past the top or left edges of the page
  # (although this code might still work in that case? idk double negatives might be trouble)
  defp fold(dots, {:up, line}) do
    Enum.map(dots, fn
      {x, y} when y < line -> {x, y}
      {x, y} -> {x, line - (y - line)}
    end)
    |> Enum.uniq()
  end

  defp fold(dots, {:left, line}) do
    Enum.map(dots, fn
      {x, y} when x < line -> {x, y}
      {x, y} -> {line - (x - line), y}
    end)
    |> Enum.uniq()
  end

  # Part 1
  def part1({dots, [fold | _]}), do: fold(dots, fold) |> length()

  # Part 2
  def part2({dots, folds}), do: Enum.reduce(folds, dots, &fold(&2, &1)) |> draw(true)

  def draw(dots, return? \\ false) do
    w = Enum.map(dots, fn {x, _} -> x end) |> Enum.max()
    h = Enum.map(dots, fn {_, y} -> y end) |> Enum.max()

    str = 
      for y <- 0..h, into: "" do
        for x <- 0..w, into: "" do
          cond do
            {x, y} in dots -> "██"
            true -> "  "
          end
        end
        |> then(&("\n" <> &1))
      end
    
    if return? do
      str
    else
      IO.puts(str)
      dots
    end
  end
end
