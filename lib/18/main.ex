defmodule AoC2021.Day18 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(&Code.string_to_quoted!/1)
  end

  # l/r are flipped because this method is meant to be called from Enum.reduce
  # (where the accumulator/running "sum" is the second param)
  defp add(r, l), do: reduce([l, r])

  # reduce the number once; then, if something changed, reduce again
  # otherwise (nothing to reduce), return the number
  defp reduce(n) do
    cond do
      n = explode?(n) -> reduce(n)
      n = split?(n) -> reduce(n)
      true -> n
    end
  end

  # splitting: returns number if splitted, nil otherwise
  defp split?([l, r]) do
    cond do
      n = split?(l) -> [n, r]
      n = split?(r) -> [l, n]
      true -> nil
    end
  end
  defp split?(n) when n > 9, do: [floor(n / 2), ceil(n / 2)]
  defp split?(_), do: nil

  # exploding: returns number if exploded, nil otherwise
  defp explode?(n), do: explode?(n, 0) |> then(fn d -> if d, do: elem(d, 0), else: nil end)
  # exploding recursive calls: return {number, [value going left, value going right]} if exploded, nil otherwise
  defp explode?([l, r], d) when d < 4 do
    cond do
      data = explode?(l, d + 1) -> explode_right(data, r)
      data = explode?(r, d + 1) -> explode_left(data, l)
      true -> nil
    end
  end
  defp explode?([l, r], _), do: {0, [l, r]}
  defp explode?(_, _), do: nil

  # if we come from the left subtree, process the value going right (rx)
  def explode_right({l, [lx, nil]}, r), do: {[l, r], [lx, nil]}
  def explode_right({l, [lx, rx]}, r), do: {[l, addl(rx, r)], [lx, nil]}
  # if we come from the right subtree, process the value going left (lx)
  def explode_left({r, [nil, rx]}, l), do: {[l, r], [nil, rx]}
  def explode_left({r, [lx, rx]}, l), do: {[addr(lx, l), r], [nil, rx]}

  # add x to the rightmost value in subtree
  defp addr(x, [l, r]), do: [l, addr(x, r)]
  defp addr(x, n), do: x + n
  # add x to the leftmost value in subtree
  defp addl(x, [l, r]), do: [addl(x, l), r]
  defp addl(x, n), do: x + n

  # calculate the number's magnitude
  def magnitude([l, r]), do: 3 * magnitude(l) + 2 * magnitude(r)
  def magnitude(n), do: n

  # Part 1
  def part1(ns), do: Enum.reduce(ns, &add/2) |> magnitude()

  # Part 2
  def part2(ns) do
    for n1 <- ns, n2 <- ns, n1 != n2 do
      add(n1, n2) |> magnitude()
    end
    |> Enum.max()
  end
end
