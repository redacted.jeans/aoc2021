defmodule AoC2021.Day11 do
  # Setup, etc.
  def setup(file) do
    for {row, y} <- File.read!(file) |> String.split("\n", trim: true) |> Enum.with_index(),
        {val, x} <- row |> String.codepoints() |> Enum.with_index(),
        into: %{} do
      {{x, y}, String.to_integer(val)}
    end
  end

  # returns a tuple {grid, num} containing the grid after the step and the number of flashes this step
  defp step(grid) do
    grid
    # increment the whole grid by one
    |> Enum.reduce(%{}, fn {coords, val}, grid ->
      Map.put(grid, coords, val + 1)
    end)
    # flash all the octopi
    |> flash([])
  end

  defp flash(grid, flashed) do
    # go through each octopus, checking whether it is flashing
    {grid, flashed} =
      Enum.reduce(grid, {grid, flashed}, fn {{x, y}, val}, {grid, flashed} ->
        # if this octopus is flashing...
        if {x, y} not in flashed and val > 9 do
          # ...increment all neighbours
          grid =
            grid
            |> Map.replace({x - 1, y - 1}, Map.get(grid, {x - 1, y - 1}, 0) + 1)
            |> Map.replace({x, y - 1}, Map.get(grid, {x, y - 1}, 0) + 1)
            |> Map.replace({x + 1, y - 1}, Map.get(grid, {x + 1, y - 1}, 0) + 1)
            |> Map.replace({x - 1, y}, Map.get(grid, {x - 1, y}, 0) + 1)
            |> Map.replace({x + 1, y}, Map.get(grid, {x + 1, y}, 0) + 1)
            |> Map.replace({x - 1, y + 1}, Map.get(grid, {x - 1, y + 1}, 0) + 1)
            |> Map.replace({x, y + 1}, Map.get(grid, {x, y + 1}, 0) + 1)
            |> Map.replace({x + 1, y + 1}, Map.get(grid, {x + 1, y + 1}, 0) + 1)

          # mark this octopus as having flashed
          {grid, [{x, y}] ++ flashed}
        else
          # otherwise, return grid and list of flashed octopi as-is
          {grid, flashed}
        end
      end)

    # check if we're done flashing (true iff no octopus has 10+ energy and hasn't flashed yet)
    done = not Enum.any?(grid, fn {coords, val} -> coords not in flashed and val > 9 end)

    if done do
      # if so, return grid (with all flashed octopi set to 0) + number of flashed octopi
      grid = Enum.reduce(flashed, grid, fn coords, grid -> Map.put(grid, coords, 0) end)
      {grid, length(flashed)}
    else
      # otherwise, repeat with updated grid + list of flashed octopi
      flash(grid, flashed)
    end
  end

  # Part 1
  # step 100 times
  def part1(grid), do: part1(grid, 0, 100)

  defp part1(_, n, 0), do: n

  defp part1(grid, n, step) do
    {grid, flashed} = step(grid)
    part1(grid, n + flashed, step - 1)
  end

  # Part 2
  def part2(grid), do: part2(grid, 1)

  # keep stepping until the number of flashed octopi is identical to the size of the grid
  defp part2(grid, n) do
    {grid, flashed} = step(grid)

    if length(Map.values(grid)) === flashed do
      n
    else
      part2(grid, n + 1)
    end
  end
end
