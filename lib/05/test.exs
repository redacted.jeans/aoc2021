defmodule AoC2021.Test05 do
  use ExUnit.Case, async: true

  alias AoC2021.Day05

  @data [
    [{0, 9}, {5, 9}],
    [{8, 0}, {0, 8}],
    [{9, 4}, {3, 4}],
    [{2, 2}, {2, 1}],
    [{7, 0}, {7, 4}],
    [{6, 4}, {2, 0}],
    [{0, 9}, {2, 9}],
    [{3, 4}, {1, 4}],
    [{0, 0}, {8, 8}],
    [{5, 5}, {8, 2}]
  ]

  test "setup" do
    assert Day05.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day05.part1(@data) === 5
  end

  test "part2" do
    assert Day05.part2(@data) === 12
  end
end
