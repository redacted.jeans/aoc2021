defmodule AoC2021.Day05 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      Enum.map(String.split(line, "->"), fn coord ->
        String.trim(coord)
        |> String.split(",")
        |> then(fn [x, y] -> {String.to_integer(x), String.to_integer(y)} end)
      end)
    end)
  end

  # Part 1
  def part1(coords) do
    Enum.flat_map(coords, fn
      [{x1, y}, {x2, y}] -> Enum.map(x1..x2, fn x -> {x, y} end)
      [{x, y1}, {x, y2}] -> Enum.map(y1..y2, fn y -> {x, y} end)
      _ -> []
    end)
    |> Enum.frequencies()
    |> Enum.count(fn {_, n} -> n >= 2 end)
  end

  # Part 2
  def part2(coords) do
    Enum.flat_map(coords, fn
      [{x1, y}, {x2, y}] -> Enum.map(x1..x2, fn x -> {x, y} end)
      [{x, y1}, {x, y2}] -> Enum.map(y1..y2, fn y -> {x, y} end)
      [{x1, y1}, {x2, y2}] -> Enum.zip([x1..x2, y1..y2])
    end)
    |> Enum.frequencies()
    |> Enum.count(fn {_, n} -> n >= 2 end)
  end
end
