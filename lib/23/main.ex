defmodule AoC2021.Day23 do
  # representing type/room as codepoints makes it easy to switch back & forth by +/- ?\s (32)
  # h [0 1 2 3 4 5 6 7 8 9 10]
  #       [0] [0] [0] [0]
  #       [1] [1] [1] [1]
  #        a   b   c   d
  @cost %{?A => 1, ?B => 10, ?C => 100, ?D => 1000}
  @exit %{?a => 2, ?b => 4, ?c => 6, ?d => 8}

  # Setup, etc.
  def setup(file) do
    [_, _, front, back, _] =
      File.read!(file)
      |> String.split("\n", trim: true)
      |> Enum.map(fn line ->
        String.replace(line, [" ", "#"], "")
        |> String.to_charlist()
      end)

    for {row, index} <- Enum.with_index([front, back]), {pod, room} <- Enum.with_index(row) do
      {pod, ?a + room, index}
    end
    |> MapSet.new()
  end

  defp search(open, costs, goal) do
    {{_, curr}, open} = Heap.split(open)
    ctd = costs[curr]

    if curr === goal do
      ctd
    else
      # get valid moves
      {open, costs} =
        neighbours(curr, ctd)
        |> Enum.filter(fn {c, p} -> not Map.has_key?(costs, p) or c < costs[p] end)
        # update "open" heap, and "costs" map
        |> Enum.reduce({open, costs}, fn {c, p}, {open, costs} ->
          {Heap.push(open, {c + h(p), p}), Map.put(costs, p, c)}
        end)

      search(open, costs, goal)
    end
  end

  defp neighbours(pods, cost) do
    for {type, room, i} <- pods do
      # still need to move if either room isn't of your type, or there are any pods of another type in your room
      todo? =
        room !== type + ?\s or Enum.any?(pods, fn p -> match?({_, ^room, _}, p) and not match?({^type, _, _}, p) end)

      cond do
        # the only valid move from the hallway are into the appropriate room
        room === ?h -> enter(i, type, pods, cost)
        # the only valid moves from within a room are to exit into the hallway
        todo? -> leave({room, i}, type, pods, cost)
        # otherwise you're in your room with no strangers, no moves
        true -> []
      end
    end
    # TODO: dedup? (probably not necessary?)
    |> List.flatten()
  end

  # src index, amphipod type, amphipod positions, cost-to-date
  defp enter(from, type, pods, cost) do
    dest = type + ?\s
    x = @exit[dest]

    # check that the path to the room's exit is clear
    clear? =
      for i <- from..x, i !== from do
        Enum.all?(pods, fn p -> not match?({_, ?h, ^i}, p) end)
      end
      |> Enum.all?()

    # check that every other pod is either in a different room or the same typ
    same? =
      Enum.all?(pods, fn p ->
        not match?({_, ^dest, _}, p) or match?({^type, _, _}, p)
      end)

    # if we have a clear path to the room's exit AND there are no 'strangers' in the room
    # move to the last available slot in dest room
    if clear? and same? do
      pods = Enum.reject(pods, &match?({_, ?h, ^from}, &1))
      # go to the deepest unnocupied slot in the room
      depth =
        Enum.reduce(pods, div(length(pods), 4), fn
          {_, ^dest, i}, d -> if i <= d, do: i - 1, else: d
          _, d -> d
        end)

      energy = (abs(x - from) + depth + 1) * @cost[type]
      {cost + energy, MapSet.new([{type, dest, depth} | pods])}
    else
      []
    end
  end

  # {src room, src index}, amphipod type, amphipod positions, cost-to-date, offset (from recursive calls)
  defp leave(src, type, pods, cost, from \\ 0)

  # if we're in the front spot in a room...
  defp leave({room, 0}, type, pods, cost, from) do
    x = @exit[room]

    # find the first available spot left and last available spot right of exit
    {l, r} =
      for {_, ?h, i} <- pods, reduce: {0, 10} do
        {l, r} when i < x -> {max(i + 1, l), r}
        {l, r} when i > x -> {l, min(i - 1, r)}
        {l, r} -> {l, r}
      end

    # generate every valid state for that range
    pods = Enum.reject(pods, &match?({_, ^room, ^from}, &1))

    for i <- l..r, i not in Map.values(@exit) do
      energy = (abs(x - i) + 1 + from) * @cost[type]
      {cost + energy, MapSet.new([{type, ?h, i} | pods])}
    end
  end

  # if we're trying to leave a room from a position other than 1
  # check if the position is occupied and recursively check the next spot
  defp leave({room, i}, type, pods, cost, from) do
    ahead = i - 1
    # if there's someone in the spot head of us, can't move
    if Enum.any?(pods, &match?({_, ^room, ^ahead}, &1)) do
      []
    else
      leave({room, ahead}, type, pods, cost, from + 1)
    end
  end

  # heuristic function
  defp h(state) do
    Enum.reduce(state, 0, fn {t, r, i}, total ->
      room = t + ?\s

      case r do
        # pod in hallway: distance to room * movement cost
        ?h -> (abs(i - @exit[room]) + 1) * @cost[t]
        # pod in own room: 0
        ^room -> 0
        # else (pod in wrong room): distance between exist * mvmt cost
        _ -> (abs(@exit[r] - @exit[room]) + 2) * @cost[t]
      end + total
    end)
  end

  # Part 1
  def part1(init) do
    goal =
      MapSet.new([
        {?A, ?a, 0}, {?B, ?b, 0}, {?C, ?c, 0}, {?D, ?d, 0},
        {?A, ?a, 1}, {?B, ?b, 1}, {?C, ?c, 1}, {?D, ?d, 1}
      ])

    Heap.min()
    |> Heap.push({0, init})
    |> search(%{init => 0}, goal)
  end

  # Part 2
  def part2(init) do
    insert = [
      {?D, ?a, 1}, {?C, ?b, 1}, {?B, ?c, 1}, {?A, ?d, 1},
      {?D, ?a, 2}, {?B, ?b, 2}, {?A, ?c, 2}, {?C, ?d, 2}
    ]

    init =
      Enum.map(init, fn
        {r, t, 1} -> {r, t, 3}
        p -> p
      end)
      |> then(&(&1 ++ insert))

    goal =
      MapSet.new([
        {?A, ?a, 0}, {?B, ?b, 0}, {?C, ?c, 0}, {?D, ?d, 0},
        {?A, ?a, 1}, {?B, ?b, 1}, {?C, ?c, 1}, {?D, ?d, 1},
        {?A, ?a, 2}, {?B, ?b, 2}, {?C, ?c, 2}, {?D, ?d, 2},
        {?A, ?a, 3}, {?B, ?b, 3}, {?C, ?c, 3}, {?D, ?d, 3}
      ])

    Heap.min()
    |> Heap.push({0, init})
    |> search(%{init => 0}, goal)
  end
end
