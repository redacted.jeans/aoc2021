defmodule AoC2021.Test23 do
  use ExUnit.Case, async: true

  alias AoC2021.Day23

  # mapset of amphipods: {type, room, position/index}
  @pods MapSet.new([
    {?B, ?a, 0}, {?C, ?b, 0}, {?B, ?c, 0}, {?D, ?d, 0},
    {?A, ?a, 1}, {?D, ?b, 1}, {?C, ?c, 1}, {?A, ?d, 1}
  ])

  test "setup" do
    assert Day23.setup("#{__DIR__}/data/test.txt") === @pods
  end

  test "part1" do
    assert Day23.part1(@pods) === 12_521
  end

  test "part2" do
    assert Day23.part2(@pods) === 44_169
  end
end
