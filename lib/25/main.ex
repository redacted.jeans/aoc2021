defmodule AoC2021.Day25 do
  # Setup, etc.
  def setup(file) do
    for {row, y} <- File.read!(file) |> String.split("\n", trim: true) |> Enum.with_index(),
        {val, x} <- String.to_charlist(row) |> Enum.with_index(),
        val !== ?., into: %{} do
      {{x, y}, val}
    end
  end

  # Part 1
  def part1(map), do: predict(map)

  defp predict(map) do
    # assuming we have a cucumber at the very right and very bottom of the map
    w = Map.keys(map) |> Enum.map(fn {x, _} -> x end) |> Enum.max()
    h = Map.keys(map) |> Enum.map(fn {_, y} -> y end) |> Enum.max()

    predict(map, {w, h}, 0)
  end

  defp predict(map, {w, h}, n) do
    cond do
      stuck?(map, {w, h}) -> n + 1
      true -> step(map, {w, h}) |> predict({w, h}, n + 1)
    end
  end

  defp stuck?(map, {w, h}) do
    Enum.all?(map, fn {pos, dir} -> move(pos, dir, {w, h}, map) === {pos, dir} end)
  end

  # FIXME: there must be a better way of doing this
  defp step(map, {w, h}) do
    # first move east
    map =
      Enum.map(map, fn {pos, dir} ->
        case dir do
          ?> -> move(pos, dir, {w, h}, map)
          _ -> {pos, dir}
        end
      end)
      |> Map.new()
    # then move south
    Enum.map(map, fn {pos, dir} ->
      case dir do
        ?v -> move(pos, dir, {w, h}, map)
        _ -> {pos, dir}
      end
    end)
    |> Map.new()
  end

  # returns the cucumber's updated (or not, if movement is blocked) position
  defp move(pos, dir, {w, h}, map) do
    case next?(pos, dir, {w, h}, map) do
      nil -> {pos, dir}
      next -> {next, dir}
    end
  end

  # given a cucumber's coordinates & direction and the map info, checks if the next spot is free
  # returns the next coords if free, nil otherwise
  defp next?({x, y}, dir, {w, h}, map) do
    next = if dir === ?>, do: {rem(x + 1, w + 1), y}, else: {x, rem(y + 1, h + 1)}
    if map[next], do: nil, else: next
  end

  # Part 2
  def part2(_), do: IO.ANSI.format([:yellow, String.duplicate("*", 50)])
end
