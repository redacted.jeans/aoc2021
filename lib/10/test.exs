defmodule AoC2021.Test10 do
  use ExUnit.Case, async: true

  alias AoC2021.Day10

  @data [
    '[({(<(())[]>[[{[]{<()<>>',
    '[(()[<>])]({[<{<<[]>>(',
    '{([(<{}[<>[]}>{[]{[(<()>',
    '(((({<>}<{<{<>}{[]{[]{}',
    '[[<[([]))<([[{}[[()]]]',
    '[{[{({}]{}}([{[{{{}}([]',
    '{<[[]]>}<{[{[{[]{()[[[]',
    '[<(<(<(<{}))><([]([]()',
    '<{([([[(<>()){}]>(<<{{',
    '<{([{{}}[<[[[<>{}]]]>[]]'
  ]

  test "setup" do
    assert Day10.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day10.part1(@data) === 26_397
  end

  test "part2" do
    assert Day10.part2(@data) === 288_957
  end
end
