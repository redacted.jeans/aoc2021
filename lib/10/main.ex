defmodule AoC2021.Day10 do
  @flipped %{?( => ?), ?[ => ?], ?{ => ?}, ?< => ?>}

  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(&to_charlist/1)
  end

  # if we have no expected chars,
  # consume the first char and parse until flipped char is found
  defp parse([char | line], []), do: parse(line, [@flipped[char]])

  # if we have expectations:
  #   first, check if char is a LHS char; if so, consume char & add matching expectation
  #   otherwise, check whether the expectation is matched; if so, consume char and expectation
  #   otherwise, this line is corrupted
  defp parse([char | line], [exp | rest]) do
    cond do
      char in Map.keys(@flipped) -> parse(line, [@flipped[char], exp | rest])
      char === exp -> parse(line, rest)
      true -> {char, :corrupted}
    end
  end

  # if we reach the end of the line and have no remaining expectations, it is complete
  defp parse('', []), do: {nil, :complete}

  # if we reach the end of the line and have remaining expectations, it is incomplete
  defp parse('', exp), do: {exp, :incomplete}

  # Part 1
  def part1(lines) do
    score = %{?) => 3, ?] => 57, ?} => 1197, ?> => 25137}

    lines
    |> Enum.map(&parse(&1, []))
    |> Enum.filter(fn {_, status} -> status === :corrupted end)
    |> Enum.reduce(0, fn {char, _}, sum -> sum + score[char] end)
  end

  # Part 2
  def part2(lines) do
    score = %{?) => 1, ?] => 2, ?} => 3, ?> => 4}

    lines
    |> Enum.map(&parse(&1, []))
    |> Enum.filter(fn {_, status} -> status === :incomplete end)
    |> Enum.map(fn {exp, _} ->
      Enum.reduce(exp, 0, fn char, acc -> acc * 5 + score[char] end)
    end)
    |> Enum.sort()
    |> then(fn scores -> Enum.at(scores, scores |> length |> div(2)) end)
  end
end
