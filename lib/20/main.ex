defmodule AoC2021.Day20 do
  use Bitwise

  # Setup, etc.
  def setup(file) do
    [alg | img] =
      File.read!(file)
      |> String.split("\n", trim: true)
      |> Enum.map(fn str ->
        String.to_charlist(str)
        |> Enum.map(fn c -> if c === ?#, do: 1, else: 0 end)
      end)

    alg = List.to_tuple(alg)

    img =
      for {row, y} <- Enum.with_index(img),
          {px, x} <- Enum.with_index(row),
          into: %{} do
        {{x, y}, px}
      end

    {alg, img}
  end

  defp enhance(img, alg, n), do: enhance(img, alg, 0, n)
  defp enhance(img, _, _, 0), do: img
  defp enhance(img, alg, default, n) do
    {{x0, y0}, {x1, y1}} = Map.keys(img) |> Enum.min_max()
    default = if match?(0, elem(alg, 0)), do: 0, else: default

    for x <- (x0 - 1)..(x1 + 1), y <- (y0 - 1)..(y1 + 1), into: %{} do
      lookup = pixel({x, y}, img, default)
      {{x, y}, elem(alg, lookup)}
    end
    |> enhance(alg, 1 - default, n - 1)
  end

  # given target {x, y} coords, the image being upsampled, and the infinite pixels' (default) value,
  # "upsamples" the given pixel to the algorithm's lookup index
  defp pixel({x, y}, img, d) do
    for y <- (y - 1)..(y + 1), x <- (x - 1)..(x + 1), reduce: 0 do
      n -> (n <<< 1) ||| Map.get(img, {x, y}, d)
    end
  end

  # Part 1
  def part1({alg, img}), do: enhance(img, alg, 2) |> Enum.count(&match?({_, 1}, &1))

  # Part 2
  def part2({alg, img}), do: enhance(img, alg, 50) |> Enum.count(&match?({_, 1}, &1))
end
