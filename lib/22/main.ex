defmodule AoC2021.Day22 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(fn
      "on " <> cuboid -> parse_line(cuboid, :on)
      "off " <> cuboid -> parse_line(cuboid, :off)
    end)
  end

  defp parse_line(cuboid, val) do
    [x, y, z] =
      Regex.scan(~r/[xyz]=(-?\d+)\.\.(-?\d+)/, cuboid, capture: :all_but_first)
      |> Enum.map(fn [n0, n1] -> {String.to_integer(n0), String.to_integer(n1)} end)

    {val, x, y, z}
  end

  defp step({action, xs, ys, zs}, reactor) do
    # take every cuboid in the reactor that overlaps with this step in the sequence and split it
    Enum.flat_map(reactor, fn cuboid ->
      cond do
        overlap?({xs, ys, zs}, cuboid) -> split(cuboid, {xs, ys, zs})
        true -> [cuboid]
      end
    end)
    # if turning cubes on, add the target cuboid to the mapped reactor
    # otherwise, return the mapped reactor as-is
    |> then(fn reactor ->
      case action do
        :on -> [{xs, ys, zs} | reactor]
        :off -> reactor
      end
    end)
  end

  # split a cuboid by another given cuboid and return the non-overlapping sub-cuboids
  # to do this, split c0 into 26 new cuboids (omitting c1, the center cuboid)
  # and discard any resulting sub-cuboids with invalid values
  defp split({{x0, x1}, {y0, y1}, {z0, z1}} = _c0, {{xa, xb}, {ya, yb}, {za, zb}} = _c1) do
    # hardcoded the 26 non-overlapping subcuboids ;(
    [
      {{x0, xa - 1}, {y0, ya - 1}, {z0, za - 1}},
      {{x0, xa - 1}, {y0, ya - 1}, {max(za, z0), min(zb, z1)}},
      {{x0, xa - 1}, {y0, ya - 1}, {zb + 1, z1}},
      {{x0, xa - 1}, {max(ya, y0), min(yb, y1)}, {z0, za - 1}},
      {{x0, xa - 1}, {max(ya, y0), min(yb, y1)}, {max(za, z0), min(zb, z1)}},
      {{x0, xa - 1}, {max(ya, y0), min(yb, y1)}, {zb + 1, z1}},
      {{x0, xa - 1}, {yb + 1, y1}, {z0, za - 1}},
      {{x0, xa - 1}, {yb + 1, y1}, {max(za, z0), min(zb, z1)}},
      {{x0, xa - 1}, {yb + 1, y1}, {zb + 1, z1}},
      {{max(xa, x0), min(xb, x1)}, {y0, ya - 1}, {z0, za - 1}},
      {{max(xa, x0), min(xb, x1)}, {y0, ya - 1}, {max(za, z0), min(zb, z1)}},
      {{max(xa, x0), min(xb, x1)}, {y0, ya - 1}, {zb + 1, z1}},
      {{max(xa, x0), min(xb, x1)}, {max(ya, y0), min(yb, y1)}, {z0, za - 1}},
      # center cuboid is just c1; don't add to list
      {{max(xa, x0), min(xb, x1)}, {max(ya, y0), min(yb, y1)}, {zb + 1, z1}},
      {{max(xa, x0), min(xb, x1)}, {yb + 1, y1}, {z0, za - 1}},
      {{max(xa, x0), min(xb, x1)}, {yb + 1, y1}, {max(za, z0), min(zb, z1)}},
      {{max(xa, x0), min(xb, x1)}, {yb + 1, y1}, {zb + 1, z1}},
      {{xb + 1, x1}, {y0, ya - 1}, {z0, za - 1}},
      {{xb + 1, x1}, {y0, ya - 1}, {max(za, z0), min(zb, z1)}},
      {{xb + 1, x1}, {y0, ya - 1}, {zb + 1, z1}},
      {{xb + 1, x1}, {max(ya, y0), min(yb, y1)}, {z0, za - 1}},
      {{xb + 1, x1}, {max(ya, y0), min(yb, y1)}, {max(za, z0), min(zb, z1)}},
      {{xb + 1, x1}, {max(ya, y0), min(yb, y1)}, {zb + 1, z1}},
      {{xb + 1, x1}, {yb + 1, y1}, {z0, za - 1}},
      {{xb + 1, x1}, {yb + 1, y1}, {max(za, z0), min(zb, z1)}},
      {{xb + 1, x1}, {yb + 1, y1}, {zb + 1, z1}}
    ]
    # filter out any cuboids with invalid (flipped) ranges
    |> Enum.filter(fn {{x0, x1}, {y0, y1}, {z0, z1}} ->
      x0 <= x1 and y0 <= y1 and z0 <= z1
    end)
  end

  # returns true if the given (3d) cuboids overlap
  defp overlap?({x0, y0, z0}, {x1, y1, z1}), do: overlap?(x0, x1) and overlap?(y0, y1) and overlap?(z0, z1)
  # returns true if the given (1d) ranges overlap
  defp overlap?({a, b}, {c, d}), do: (a <= c and c <= b) or (c <= a and a <= d)

  # given a list of cuboids, calculate how many cubes are in it
  defp volume(ranges) do
    Enum.reduce(ranges, 0, fn {{x0, x1}, {y0, y1}, {z0, z1}}, sum ->
      sum + (x1 - x0 + 1) * (y1 - y0 + 1) * (z1 - z0 + 1)
    end)
  end

  # Part 1
  def part1([{:on, x, y, z} | seq]) do
    # assuming every range is either entirely within or entirely without the -50..50 cuboid,
    # we only need to check a single value
    Enum.filter(seq, fn {_, {x0, _}, _, _} -> -50 <= x0 and x0 <= 50 end)
    |> Enum.reduce([{x, y, z}], &step/2)
    |> volume()
  end

  # Part 2
  def part2([{:on, x, y, z} | seq]) do
    Enum.reduce(seq, [{x, y, z}], &step/2)
    |> volume()
  end
end
