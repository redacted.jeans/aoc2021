defmodule AoC2021.Test16 do
  use ExUnit.Case, async: true

  alias AoC2021.Day16

  # uniquely amongst all days, I'm not testing setup
  # since all it does is read the file and trim it
  # (also i'm too lazy to create 12 test files)
  test "setup" do end

  @p1 "8A004A801A8002F478"
  @p2 "620080001611562C8802118E34"
  @p3 "C0015000016115A2E0802F182340"
  @p4 "A0016C880162017C3686B18A3D4780"

  test "part1" do
    assert Day16.part1(@p1) === 16
    assert Day16.part1(@p2) === 12
    assert Day16.part1(@p3) === 23
    assert Day16.part1(@p4) === 31
  end

  @p5 "C200B40A82"
  @p6 "04005AC33890"
  @p7 "880086C3E88112"
  @p8 "CE00C43D881120"
  @p9 "D8005AC2A8F0"
  @p10 "F600BC2D8F"
  @p11 "9C005AC2F8F0"
  @p12 "9C0141080250320F1802104A08"

  test "part2" do
    assert Day16.part2(@p5) === 3
    assert Day16.part2(@p6) === 54
    assert Day16.part2(@p7) === 7
    assert Day16.part2(@p8) === 9
    assert Day16.part2(@p9) === 1
    assert Day16.part2(@p10) === 0
    assert Day16.part2(@p11) === 0
    assert Day16.part2(@p12) === 1
  end
end
