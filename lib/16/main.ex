defmodule AoC2021.Day16 do
  # Setup, etc.
  def setup(file) do
    File.read!(file) |> String.trim()
  end

  # returns a tuple containing {version #, type ID, packet data}
  # this is just a wrapper around parse_once(...) which converts the data to a bitstring
  # and discards unparsed bits
  defp parse(data), do: parse_once(:binary.decode_hex(data)) |> then(fn {p, _} -> p end)

  # parse a single packet and return {packet, rest}
  # a packet comprises:
  # - a 3-bit version number
  # - 1 3-bit type ID (4: literal, _: operator)
  # - the packet data
  defp parse_once(<<v::3, 4::3, rest::bits>>) do
    {data, rest} = parse_lit(rest, [])
    {{v, 4, data}, rest}
  end
  defp parse_once(<<v::3, t::3, rest::bits>>) do
    {data, rest} = parse_op(rest)
    {{v, t, data}, rest}
  end

  # keep parsing contiguous packets until data is exhausted
  # (mainly a helper for parse_op when length type ID === 0)
  defp parse_all(<<>>, packets), do: Enum.reverse(packets)
  defp parse_all(data, packets) do
    {packet, rest} = parse_once(data)
    parse_all(rest, [packet | packets])
  end

  # parse exactly the given number of packets
  # (mainly a helper for parse_op when length type ID === 1)
  defp parse_until(rest, packets, 0), do: {Enum.reverse(packets), rest}
  defp parse_until(data, packets, n) do
    {packet, rest} = parse_once(data)
    parse_until(rest, [packet | packets], n - 1)
  end

  # a literal comprises:
  # - one bit indicating whether to continue parsing (1: keep going, 0: last group)
  # - 4 bits containing the current group
  defp parse_lit(<<0::1, n::4, rest::bits>>, val), do: {bits_to_int([n | val]), rest}
  defp parse_lit(<<1::1, n::4, rest::bits>>, val), do: parse_lit(rest, [n | val])

  # an operator comprises:
  # - one bit indicating the length type ID (0: next 15 bits, 1: next 11 bits)
  # - 11 or 15 bits representing the number/size of packets operated on
  # - the sub-packets
  defp parse_op(<<0::1, l::15, data::bits-size(l), rest::bits>>), do: {parse_all(data, []), rest}
  defp parse_op(<<1::1, l::11, rest::bits>>), do: parse_until(rest, [], l)

  # converts a group of literal bits into the integer it represents
  # each group is a 4-bit sequence that needs to be concatenated to the rest, in reverse order
  defp bits_to_int(bit_groups) do
    bit_groups
    |> Enum.map(&(Integer.to_string(&1, 2) |> String.pad_leading(4, "0")))
    |> Enum.reduce(&(&1 <> &2))
    |> String.to_integer(2)
  end

  # Part 1
  def part1(packet), do: packet |> parse() |> vsum()

  # returns the sum of all packet versions
  defp vsum({v, _, data}) when not is_list(data), do: v
  defp vsum({v, _, data}), do: Enum.reduce(data, v, fn packet, v -> v + vsum(packet) end)

  # Part 2
  def part2(packet), do: packet |> parse() |> evaluate()

  # given a parsed/decoded packet, evaluate the expression it represents
  defp evaluate({_, 0, data}), do: Enum.map(data, &evaluate/1) |> Enum.sum()
  defp evaluate({_, 1, data}), do: Enum.map(data, &evaluate/1) |> Enum.product()
  defp evaluate({_, 2, data}), do: Enum.map(data, &evaluate/1) |> Enum.min()
  defp evaluate({_, 3, data}), do: Enum.map(data, &evaluate/1) |> Enum.max()
  defp evaluate({_, 4, n}), do: n
  defp evaluate({_, 5, data}), do: Enum.map(data, &evaluate/1) |> then(fn [p1, p2] -> if p1 > p2, do: 1, else: 0 end)
  defp evaluate({_, 6, data}), do: Enum.map(data, &evaluate/1) |> then(fn [p1, p2] -> if p1 < p2, do: 1, else: 0 end)
  defp evaluate({_, 7, data}), do: Enum.map(data, &evaluate/1) |> then(fn [p1, p2] -> if p1 === p2, do: 1, else: 0 end)
end
