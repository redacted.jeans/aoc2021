defmodule AoC2021.Day15 do
  # Setup, etc.
  def setup(file) do
    file = File.read!(file)

    for {line, y} <- String.split(file, "\n", trim: true) |> Enum.with_index(),
        {node, x} <- String.trim(line) |> String.codepoints() |> Enum.with_index(),
        into: %{} do
      {{x, y}, String.to_integer(node)}
    end
  end

  defp find_goal(map) do
    # goal is a tuple of the highest x value & highest y value
    goal =
      Enum.reduce(map, {0, 0}, fn {{x1, y1}, _}, {x2, y2} ->
        {max(x1, x2), max(y1, y2)}
      end)

    {map, goal}
  end

  # A* (i think)
  # if we exhaust the search space without finding a path, this will probably crash
  defp search(open, costs, {mx, my} = goal, map) do
    # get current node & cost-to-date
    {{_, x, y}, open} = Heap.split(open)
    ctd = costs[{x, y}]

    # if node is goal, return cost-to-date
    if {x, y} === goal do
      ctd
    else
      # get valid neighbours + their h score
      neighbours =
        [{x, y - 1}, {x, y + 1}, {x - 1, y}, {x + 1, y}]
        |> Enum.reject(fn {x, y} -> x < 0 or y < 0 or x > mx or y > my or Map.has_key?(costs, {x, y}) end)
        |> Enum.map(fn {x, y} -> {ctd + map[{x, y}] + h({x, y}, goal), x, y} end)

      # add them to the open set (sorted by h score)
      open = Enum.reduce(neighbours, open, &Heap.push(&2, &1))

      # mark neighbours as seen & store their costs
      costs =
        Enum.reduce(neighbours, costs, fn {_, x, y}, seen ->
          Map.put(seen, {x, y}, ctd + map[{x, y}])
        end)

      # go to next node in frontier
      search(open, costs, goal, map)
    end
  end

  # heuristic function: manhattan distance
  defp h({x1, y1}, {x2, y2}), do: (x2 - x1) + (y2 - y1)

  # Part 1
  def part1(map) do
    {map, goal} = find_goal(map)

    # open set is stored as list of tuples {h, x, y}
    Heap.min() |> Heap.push({0, 0, 0}) |> search(%{{0, 0} => 0}, goal, map)
  end

  # Part 2
  def part2(map) do
    # expand the map five times in each direction & get goal
    {map, goal} = expand(map) |> find_goal()

    # open set is stored as list of tuples {h, x, y}
    Heap.min() |> Heap.push({0, 0, 0}) |> search(%{{0, 0} => 0}, goal, map)
  end

  defp expand(map) do
    {tw, th} =
      Enum.reduce(map, {0, 0}, fn {{x1, y1}, _}, {x2, y2} ->
        {max(x1 + 1, x2), max(y1 + 1, y2)}
      end)

    for ty <- 0..4, tx <- 0..4 do
      # make a new map with x & y coords incremented by tx * tw, ty * th
      # and cost/risk incremented by tx + ty (wrapping back to 1 at 10)
      Enum.reduce(map, %{}, fn {{x, y}, c}, m ->
        c =
          (c + tx + ty)
          |> Integer.mod(9)
          |> then(fn c -> if c === 0, do: 9, else: c end)

        Map.put(m, {x + tw * tx, y + th * ty}, c)
      end)
    end
    |> Enum.reduce(&Map.merge/2)
  end
end
