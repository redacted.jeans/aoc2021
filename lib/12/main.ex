defmodule AoC2021.Day12 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.reduce(%{}, fn line, graph ->
      [cave1, cave2] = String.split(line, "-")

      graph
      |> Map.update(cave1, [cave2], &[cave2 | &1])
      |> Map.update(cave2, [cave1], &[cave1 | &1])
    end)
  end

  defp large?(<<char, _::binary>>), do: char <= ?Z

  # init: start as single visited cave
  defp search(graph, double?), do: search("start", [], graph, double?)

  # if current cave is end, we've found a valid path
  defp search("end", _, _, _), do: 1

  defp search(cave, visited, graph, double?) do
    # double-visiting is enabled iff:
    # 1. it's already on (can't be turned on from off)
    # 2. this cave is either large or not already visited
    double? = double? and (large?(cave) or cave not in visited)

    # get valid neighbours and search them
    graph[cave]
    |> Enum.reduce(0, fn next, total ->
      total + cond do
        # can't visit start again
        next === "start" -> 0
        # if doubling is on or cave is large, can always visit
        double? or large?(next) -> search(next, [cave | visited], graph, double?)
        # otherwise (small cave, doubling off), can't visit again
        next in visited -> 0
        # if no match, visiting a small cave for the first time
        true -> search(next, [cave | visited], graph, double?)
      end
    end)
  end

  # Part 1
  def part1(graph), do: search(graph, false)

  # Part 2
  def part2(graph), do: search(graph, true)
end
