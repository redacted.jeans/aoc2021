defmodule AoC2021.Test12 do
  use ExUnit.Case, async: true

  alias AoC2021.Day12

  # represent the graph as an adjacency list (note that each edge is represented twice)
  @small %{
    "start" => ["b", "A"],
    "A" => ["end", "b", "c", "start"],
    "b" => ["end", "d", "A", "start"],
    "c" => ["A"],
    "d" => ["b"],
    "end" => ["b", "A"]
  }

  @medium %{
    "start" => ["dc", "kj", "HN"],
    "HN" => ["kj", "end", "dc", "start"],
    "LN" => ["dc"],
    "dc" => ["kj", "LN", "HN", "start", "end"],
    "kj" => ["dc", "HN", "sa", "start"],
    "sa" => ["kj"],
    "end" => ["HN", "dc"]
  }

  @large %{
    "start" => ["RW", "pj", "DX"],
    "DX" => ["fs", "pj", "start", "he"],
    "RW" => ["start", "zg", "pj", "he"],
    "WI" => ["he"],
    "fs" => ["pj", "DX", "he", "end"],
    "he" => ["zg", "WI", "RW", "pj", "fs", "DX"],
    "pj" => ["fs", "start", "RW", "he", "zg", "DX"],
    "sl" => ["zg"],
    "zg" => ["he", "RW", "pj", "sl", "end"],
    "end" => ["zg", "fs"]
  }

  test "setup" do
    assert Day12.setup("#{__DIR__}/data/small.txt") === @small
    assert Day12.setup("#{__DIR__}/data/medium.txt") === @medium
    assert Day12.setup("#{__DIR__}/data/large.txt") === @large
  end

  test "part1" do
    assert Day12.part1(@small) === 10
    assert Day12.part1(@medium) === 19
    assert Day12.part1(@large) === 226
  end

  test "part2" do
    assert Day12.part2(@small) === 36
    assert Day12.part2(@medium) === 103
    assert Day12.part2(@large) === 3_509
  end
end