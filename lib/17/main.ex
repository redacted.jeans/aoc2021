defmodule AoC2021.Day17 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> then(&Regex.run(~r/x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)/, &1))
    |> then(fn [_ | rest] -> rest end)
    |> Enum.map(&String.to_integer/1)
    |> then(fn [x0, x1, y0, y1] -> {{x0, x1}, {y0, y1}} end)
  end

  # naive solution that simply checks every possible velocity from {0, min_y} to {max_x, -min_y}
  # imho the reason these numbers are correct is intuitive but not obvious:
  # x >= 0 is obvious, since if we shoot the probe backwards it will never reach the target
  # x <= max_x is obvious, since anything greater than that will immediately overshoot
  # y >= min_y is obvious, since anything shot below that will immediately sink too low
  # y <= -min_y is less obvious, but anything shot higher will overshoot as it falls back down
  defp find_arcs({{_, max_x}, {min_y, _}} = target) do
    for vx <- 0..max_x,
        vy <- min_y..-min_y,
        is_list(path = trajectory({vx, vy}, target)) do
      path
    end
  end

  # if no coords or path are provided, initialize with {0, 0}
  defp trajectory(vs, target), do: trajectory({0, 0}, vs, target, [])
  # probe is further right or further down than target: overshot, return nil
  defp trajectory({x, y}, _, {{_, max_x}, {min_y, _}}, _) when x > max_x or y < min_y, do: nil
  # probe is in target: ok, return path
  defp trajectory({x, y}, _, {{min_x, max_x}, {min_y, max_y}}, arc)
       when min_x <= x and x <= max_x and min_y <= y and y <= max_y,
       do: [{x, y} | arc]
  # otherwise, move and adjust velocity
  defp trajectory({x, y}, {vx, vy}, target, arc) do
    cond do
      vx > 0 -> trajectory({x + vx, y + vy}, {vx - 1, vy - 1}, target, [{x, y} | arc])
      vx < 0 -> trajectory({x + vx, y + vy}, {vx + 1, vy - 1}, target, [{x, y} | arc])
      true -> trajectory({x, y + vy}, {vx, vy - 1}, target, [{x, y} | arc])
    end
  end

  # Part 1
  def part1(target), do: find_arcs(target) |> Enum.map(&peak/1) |> Enum.max()

  defp peak(path), do: Enum.map(path, fn {_, y} -> y end) |> Enum.max()

  # Part 2
  def part2(target), do: find_arcs(target) |> length()
end
