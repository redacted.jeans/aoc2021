defmodule AoC2021.Test17 do
  use ExUnit.Case, async: true

  alias AoC2021.Day17

  # {{min x, max x}, {min y, max y}}
  @data {{20, 30}, {-10, -5}}

  test "setup" do
    assert Day17.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day17.part1(@data) === 45
  end

  test "part2" do
    assert Day17.part2(@data) === 112
  end
end
