defmodule AoC2021.Day04 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> then(fn [draws | boards] ->
      %{
        :draws => String.split(draws, ",", trim: true),
        :boards =>
          Enum.map(boards, &String.split(&1, " ", trim: true))
          |> Enum.chunk_every(5)
      }
    end)
  end

  defp update(board, val) do
    Enum.map(board, fn row ->
      Enum.map(row, fn
        ^val -> nil
        num -> num
      end)
    end)
  end

  defp check(board) do
    # if any row or column is entirely composed of nils, this board is a winner
    rows =
      board
      |> Enum.any?(&Enum.all?(&1, fn sq -> sq === nil end))

    cols =
      board
      |> Enum.zip_with(&Enum.all?(&1, fn sq -> sq === nil end))
      |> Enum.any?()

    rows or cols
  end

  # Part 1
  def part1(%{:draws => draws, :boards => boards}) do
    {winner, last} = draw_winner(boards, draws)

    winner
    |> List.flatten()
    |> Enum.reduce(0, fn
      nil, acc -> acc
      num, acc -> acc + String.to_integer(num)
    end)
    |> then(fn sum -> sum * String.to_integer(last) end)
  end

  defp draw_winner(boards, [cur | remain]) do
    # 1. replace all occurences of num with nil
    boards = Enum.map(boards, &update(&1, cur))

    # 2. check boards for a winner (nil if none found)
    winner = Enum.find(boards, &check/1)

    # 3. if winner, return winner & current draw; otherwise recurse
    case winner do
      nil -> draw_winner(boards, remain)
      _ -> {winner, cur}
    end
  end

  # Part 2
  def part2(%{:draws => draws, :boards => boards}) do
    {loser, last} = draw_loser(boards, draws)

    loser
    |> List.flatten()
    |> Enum.reduce(0, fn
      nil, acc -> acc
      num, acc -> acc + String.to_integer(num)
    end)
    |> then(fn sum -> sum * String.to_integer(last) end)
  end

  defp draw_loser([board], remain) do
    solve(board, remain)
  end

  defp draw_loser(boards, [cur | remain]) do
    # 1. replace all occurences of num with nil
    boards =
      Enum.map(boards, &update(&1, cur))
      |> Enum.filter(fn board -> not check(board) end)

    draw_loser(boards, remain)
  end

  # keep going through the draws until the given board is solved
  defp solve(board, [cur | remain]) do
    board = update(board, cur)

    cond do
      check(board) -> {board, cur}
      true -> solve(board, remain)
    end
  end
end
