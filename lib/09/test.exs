defmodule AoC2021.Test09 do
  use ExUnit.Case, async: true

  alias AoC2021.Day09

  @data %{
    0 => %{0 => 2, 1 => 1, 2 => 9, 3 => 9, 4 => 9, 5 => 4, 6 => 3, 7 => 2, 8 => 1, 9 => 0},
    1 => %{0 => 3, 1 => 9, 2 => 8, 3 => 7, 4 => 8, 5 => 9, 6 => 4, 7 => 9, 8 => 2, 9 => 1},
    2 => %{0 => 9, 1 => 8, 2 => 5, 3 => 6, 4 => 7, 5 => 8, 6 => 9, 7 => 8, 8 => 9, 9 => 2},
    3 => %{0 => 8, 1 => 7, 2 => 6, 3 => 7, 4 => 8, 5 => 9, 6 => 6, 7 => 7, 8 => 8, 9 => 9},
    4 => %{0 => 9, 1 => 8, 2 => 9, 3 => 9, 4 => 9, 5 => 6, 6 => 5, 7 => 6, 8 => 7, 9 => 8}
  }

  test "setup" do
    assert Day09.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day09.part1(@data) === 15
  end

  test "part2" do
    assert Day09.part2(@data) === 1_134
  end
end
