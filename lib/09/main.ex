defmodule AoC2021.Day09 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.reduce(%{}, fn {row, y}, map ->
      String.codepoints(row)
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {cell, x}, map ->
        Map.put(map, x, String.to_integer(cell))
      end)
      |> then(&Map.put(map, y, &1))
    end)
  end

  # Returns a list of tuples {x, y} of all the low points in grid.
  defp low_points(grid) do
    grid
    |> Enum.flat_map(fn {y, row} ->
      row
      |> Enum.flat_map(fn {x, cell} ->
        low =
          grid[y - 1][x] > cell and grid[y + 1][x] > cell and
            grid[y][x - 1] > cell and grid[y][x + 1] > cell

        cond do
          low -> [{x, y}]
          true -> []
        end
      end)
    end)
  end

  # Part 1
  def part1(grid) do
    grid
    |> low_points()
    |> Enum.reduce(0, fn {x, y}, risk -> risk + grid[y][x] + 1 end)
  end

  # Part 2
  def part2(grid) do
    grid
    |> low_points()
    |> Enum.map(&bassin([&1], [], grid))
    |> Enum.sort(:desc)
    |> then(fn [b1, b2, b3 | _] -> b1 * b2 * b3 end)
  end

  defp bassin([{x, y} | boundary], explored, grid) do
    neighbours =
      [{x, y - 1}, {x, y + 1}, {x - 1, y}, {x + 1, y}]
      |> Enum.filter(fn {x, y} ->
        {x, y} not in boundary and {x, y} not in explored and grid[y][x] < 9
      end)

    bassin(boundary ++ neighbours, neighbours ++ explored, grid)
  end

  defp bassin([], explored, _), do: Enum.count(explored)
end
