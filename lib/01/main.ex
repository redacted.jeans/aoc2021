defmodule AoC2021.Day01 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer(&1))
  end

  # Part 1
  def part1(nums), do: part1(nums, 0)

  defp part1([first, second | rest], count) when second > first do
    part1([second | rest], count + 1)
  end

  defp part1([_ | rest], count), do: part1(rest, count)

  defp part1([], count), do: count

  # Part 2
  def part2(nums), do: part2(nums, 0)

  defp part2([first, second, third, fourth | rest], count) when fourth > first do
    part2([second, third, fourth | rest], count + 1)
  end

  defp part2([_ | rest], count), do: part2(rest, count)

  defp part2([], count), do: count
end
