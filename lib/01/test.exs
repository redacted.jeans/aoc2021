defmodule AoC2021.Test01 do
  use ExUnit.Case, async: true

  alias AoC2021.Day01

  @data [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

  test "setup" do
    assert Day01.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day01.part1(@data) === 7
  end

  test "part2" do
    assert Day01.part2(@data) === 5
  end
end
