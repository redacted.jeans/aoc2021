defmodule AoC2021.Test24 do
  use ExUnit.Case, async: true

  alias AoC2021.Day24

  @small [
    [:inp, :x],
    [:mul, :x, -1]
  ]
  @medium [
    [:inp, :z],
    [:inp, :x],
    [:mul, :z, 3],
    [:eql, :z, :x]
  ]
  @large [
    [:inp, :w],
    [:add, :z, :w],
    [:mod, :z, 2],
    [:div, :w, 2],
    [:add, :y, :w],
    [:mod, :y, 2],
    [:div, :w, 2],
    [:add, :x, :w],
    [:mod, :x, 2],
    [:div, :w, 2],
    [:mod, :w, 2]
  ]

  test "setup" do
    assert Day24.setup("#{__DIR__}/data/small.txt") === @small
    assert Day24.setup("#{__DIR__}/data/medium.txt") === @medium
    assert Day24.setup("#{__DIR__}/data/large.txt") === @large
    # also testing the interpreter here, since we don't have a better place for that
    assert Day24.run(@small, [1]) === %{:w => 0, :x => -1, :y => 0, :z => 0, :in => []}
    assert Day24.run(@medium, [2, 6]) === %{:w => 0, :x => 6, :y => 0, :z => 1, :in => []}
    assert Day24.run(@large, [7]) === %{:w => 0, :x => 1, :y => 1, :z => 1, :in => []}
  end

  # since we have no example inputs for parts 1 or 2, no tests
  test "part1" do end

  test "part2" do end
end
