defmodule AoC2021.Day24 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      String.split(line)
      |> Enum.map(fn token ->
        if String.match?(token, ~r/-?\d+/) do
          String.to_integer(token)
        else
          # all atoms should exist because they're defined below
          String.to_existing_atom(token)
        end
      end)
    end)
  end

  # reads an input value and writes it to a
  defp exec(alu, :inp, [a]) do
    [input | rest] = alu.in
    alu |> Map.put(a, input) |> Map.put(:in, rest)  
  end

  # adds a to b, then stores the result in a
  defp exec(alu, :add, [a, b]) when is_atom(b), do: exec(alu, :add, [a, alu[b]])
  defp exec(alu, :add, [a, b]), do: Map.put(alu, a, alu[a] + b)

  # multiplies a by b, then stores the result in a
  defp exec(alu, :mul, [a, b]) when is_atom(b), do: exec(alu, :mul, [a, alu[b]])
  defp exec(alu, :mul, [a, b]), do: Map.put(alu, a, alu[a] * b)

  # divides a by b, truncates it, then stores in a
  defp exec(alu, :div, [a, b]) when is_atom(b), do: exec(alu, :div, [a, alu[b]])
  defp exec(alu, :div, [a, b]), do: Map.put(alu, a, div(alu[a], b))

  # divides a by b, then stores in a
  # (since values are never <=0, no need to worry about rem/2 vs Integer.mod/2)
  defp exec(alu, :mod, [a, b]) when is_atom(b), do: exec(alu, :mod, [a, alu[b]])
  defp exec(alu, :mod, [a, b]), do: Map.put(alu, a, rem(alu[a], b))

  # if a === b, stores 1 in a; otherwise stores 0 in a
  defp exec(alu, :eql, [a, b]) when is_atom(b), do: exec(alu, :eql, [a, alu[b]])
  defp exec(alu, :eql, [a, b]), do: Map.put(alu, a, if(alu[a] === b, do: 1, else: 0))

  # initializes the alu with the given input and runs the program through it line-by-line
  def run(program, input) do
    alu = %{:w => 0, :x => 0, :y => 0, :z => 0, :in => input}
    Enum.reduce(program, alu, fn [op | params], alu -> exec(alu, op, params) end)
  end

  # for a good explanation of what's going on here, see:
  #   https://github.com/dphilipson/advent-of-code-2021/blob/master/src/days/day24.rs
  # returns a list of requirements {lhs, rhs, num} where lhs = rhs + num
  defp solve(monad), do: Enum.chunk_every(monad, 18) |> Enum.with_index() |> solve([], [])

  defp solve([], [], reqs), do: reqs

  defp solve([{chunk, n} | monad], stack, reqs) do
    if Enum.at(chunk, 4) === [:div, :z, 1] do
      [:add, :y, offset] = Enum.at(chunk, 15)
      solve(monad, [{n, offset} | stack], reqs)
    else
      [:add, :x, check] = Enum.at(chunk, 5)
      [{eq, offset} | stack] = stack
      solve(monad, stack, [{n, eq, check + offset} | reqs])
    end
  end

  # Part 1
  def part1(monad), do: solve(monad) |> maximize()

  defp maximize(reqs) do
    len = length(reqs) * 2 - 1

    Enum.reduce(reqs, 0, fn {lhs, rhs, off}, n ->
      if off >= 0 do
        n + 9 * Integer.pow(10, len - lhs) + (9 - off) * Integer.pow(10, len - rhs)
      else
        n + (9 + off) * Integer.pow(10, len - lhs) + 9 * Integer.pow(10, len - rhs)
      end
    end)
  end

  # Part 2
  def part2(monad), do: solve(monad) |> minimize()

  defp minimize(reqs) do
    len = length(reqs) * 2 - 1

    Enum.reduce(reqs, 0, fn {lhs, rhs, off}, n ->
      if off >= 0 do
        n + (1 + off) * Integer.pow(10, len - lhs) + 1 * Integer.pow(10, len - rhs)
      else
        n + 1 * Integer.pow(10, len - lhs) + (1 - off) * Integer.pow(10, len - rhs)
      end
    end)
  end
end
