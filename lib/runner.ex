defmodule Mix.Tasks.Day do
  use Mix.Task

  def run([day]), do: run([day, "lib/#{day}/data/input.txt"])

  def run([day, file]) do
    module = String.to_existing_atom("Elixir.AoC2021.Day#{day}")
    data = module.setup(file)
    IO.puts("Part 1: #{module.part1(data)}")
    IO.puts("Part 2: #{module.part2(data)}")
  end

  def run(_), do: IO.puts(:stderr, "Usage: mix day NUM [input]")
end
