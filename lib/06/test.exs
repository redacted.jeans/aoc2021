defmodule AoC2021.Test06 do
  use ExUnit.Case, async: true

  alias AoC2021.Day06

  @data [3, 4, 3, 1, 2]

  test "setup" do
    assert Day06.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day06.part1(@data) === 5934
  end

  test "part2" do
    assert Day06.part2(@data) === 26_984_457_539
  end
end
