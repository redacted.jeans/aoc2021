defmodule AoC2021.Day06 do
  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split([",", "\n"], trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  defp breed(fish, 0), do: fish

  defp breed(fish, gen) do
    fish =
      Enum.reduce(fish, %{}, fn {age, num}, next ->
        case age do
          0 -> next |> Map.update(8, num, &(&1 + num)) |> Map.update(6, num, &(&1 + num))
          _ -> next |> Map.update(age - 1, num, &(&1 + num))
        end
      end)

    breed(fish, gen - 1)
  end

  # Part 1
  def part1(init) do
    Enum.frequencies(init)
    |> breed(80)
    |> Enum.reduce(0, fn {_, val}, sum -> sum + val end)
  end

  # Part 2
  def part2(init) do
    Enum.frequencies(init)
    |> breed(256)
    |> Enum.reduce(0, fn {_, val}, sum -> sum + val end)
  end
end
