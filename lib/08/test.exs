defmodule AoC2021.Test08 do
  use ExUnit.Case, async: true

  alias AoC2021.Day08

  @small [
    [['acedgfb', 'cdfbe', 'gcdfa', 'fbcad', 'dab', 'cefabd', 'cdfgeb', 'eafb', 'cagedb', 'ab'],
     ['cdfeb', 'fcadb', 'cdfeb', 'cdbaf']]
  ]

  @large [
    [['be', 'cfbegad', 'cbdgef', 'fgaecd', 'cgeb', 'fdcge', 'agebfd', 'fecdb', 'fabcd', 'edb'],
     ['fdgacbe', 'cefdb', 'cefbgd', 'gcbe']],
    [['edbfga', 'begcd', 'cbg', 'gc', 'gcadebf', 'fbgde', 'acbgfd', 'abcde', 'gfcbed', 'gfec'],
     ['fcgedb', 'cgb', 'dgebacf', 'gc']],
    [['fgaebd', 'cg', 'bdaec', 'gdafb', 'agbcfd', 'gdcbef', 'bgcad', 'gfac', 'gcb', 'cdgabef'],
     ['cg', 'cg', 'fdcagb', 'cbg']],
    [['fbegcd', 'cbd', 'adcefb', 'dageb', 'afcb', 'bc', 'aefdc', 'ecdab', 'fgdeca', 'fcdbega'],
     ['efabcd', 'cedba', 'gadfec', 'cb']],
    [['aecbfdg', 'fbg', 'gf', 'bafeg', 'dbefa', 'fcge', 'gcbea', 'fcaegb', 'dgceab', 'fcbdga'],
     ['gecf', 'egdcabf', 'bgf', 'bfgea']],
    [['fgeab', 'ca', 'afcebg', 'bdacfeg', 'cfaedg', 'gcfdb', 'baec', 'bfadeg', 'bafgc', 'acf'],
     ['gebdcfa', 'ecba', 'ca', 'fadegcb']],
    [['dbcfg', 'fgd', 'bdegcaf', 'fgec', 'aegbdf', 'ecdfab', 'fbedc', 'dacgb', 'gdcebf', 'gf'],
     ['cefg', 'dcbef', 'fcge', 'gbcadfe']],
    [['bdfegc', 'cbegaf', 'gecbf', 'dfcage', 'bdacg', 'ed', 'bedf', 'ced', 'adcbefg', 'gebcd'],
     ['ed', 'bcgafe', 'cdgba', 'cbgef']],
    [['egadfb', 'cdbfeg', 'cegd', 'fecab', 'cgb', 'gbdefca', 'cg', 'fgcdab', 'egfdb', 'bfceg'],
     ['gbdfcae', 'bgc', 'cg', 'cgb']],
    [['gcafb', 'gcf', 'dcaebfg', 'ecagb', 'gf', 'abcdeg', 'gaef', 'cafbge', 'fdbac', 'fegbdc'],
     ['fgae', 'cfgab', 'fg', 'bagce']]
  ]

  test "setup" do
    assert Day08.setup("#{__DIR__}/data/small.txt") === @small
    assert Day08.setup("#{__DIR__}/data/large.txt") === @large
  end

  test "part1" do
    assert Day08.part1(@small) === 0
    assert Day08.part1(@large) === 26
  end

  test "part2" do
    assert Day08.part2(@small) ===  5_353
    assert Day08.part2(@large) === 61_229
  end
end
