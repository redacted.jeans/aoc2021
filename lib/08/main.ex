defmodule AoC2021.Day08 do
  # I use numbers to denote a signal's "correct" position on the 7-seg display:
  # +-0-+
  # 5   1
  # +-6-+
  # 4   2
  # +-3-+
  # this makes it easy to differentiate between the canonical and scrambled positions
  # (0..6 and a..g respectively)
  @lookup %{
    '012345' => "0",
    '12' => "1",
    '01346' => "2",
    '01236' => "3",
    '1256' => "4",
    '02356' => "5",
    '023456' => "6",
    '012' => "7",
    '0123456' => "8",
    '012356' => "9"
  }

  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split(["\n", "|"], trim: true)
    |> Enum.map(fn s ->
      String.split(s)
      |> Enum.map(&String.to_charlist/1)
    end)
    |> Enum.chunk_every(2)
  end

  # Part 1
  def part1(entries) do
    entries
    |> Enum.flat_map(fn [_, outs] ->
      Enum.filter(outs, fn out -> length(out) in [2, 3, 4, 7] end)
    end)
    |> Enum.count()
  end

  # Part 2
  def part2(entries) do
    # map of potential values per signal length
    potential = Enum.reduce(@lookup, %{}, fn {sig, _}, map ->
      Map.update(map, length(sig), [sig], fn sigs -> [sig] ++ sigs  end) 
    end)

    Enum.map(entries, fn [signals, outs] ->
      # build constraint list, solve it, and decode the outputs based on the result
      Enum.map(signals, &{&1, potential[length(&1)]})
      |> solve()
      |> decode(outs)
    end)
    |> Enum.sum()
  end

  defp solve(rules) do
    # run each rule over the rest of the domain
    # then filter out any empty rules
    rules =
      rules
      |> Enum.reduce(rules, &apply_rule/2)
      |> Enum.filter(fn {k, _} -> length(k) > 0 end)

    # the problem is solved once every rule in the constraint list maps a single char to a single
    # rule (each rule is the same length as the key, so this rule will necessarily be a single
    # position)
    cond do
      Enum.any?(rules, fn {char, pos} -> length(char) > 1 or length(pos) > 1 end) -> solve(rules)
      # turn the list of constraints into a %{char => pos} map
      # NOTE: there likely will be duplicate rules since we don't dedup above, but that doesn't
      # really matter since they should be identical (both key & val)
      true -> Enum.reduce(rules, %{}, fn {[char], [[pos]]}, map -> Map.put(map, char, pos) end)
    end
  end

  # only apply a constraint if it has one possible rule,
  # otherwise we might apply the wrong one
  defp apply_rule({key, [rule]}, domain) do
    Enum.map(domain, fn {vars, vals} ->
      cond do
        # don't apply the rule to itself (it would always cancel out)
        key === vars and [rule] === vals ->
          {vars, vals}

        # if the key fits in vars, subtract the rule from this constraint
        # (remove rules that can't be subtracted, as they can't satisfy the rule)
        key -- vars === [] ->
          {vars -- key,
           Enum.filter(vals, fn val -> rule -- val === [] end)
           |> Enum.map(fn val -> val -- rule end)}

        # if the rule doesn't match, don't apply it
        true ->
          {vars, vals}
      end
    end)
  end

  # if a constraint has more than one possible rule, skip it
  defp apply_rule(_, domain), do: domain

  defp decode(key, outs) do
    Enum.map(outs, fn out ->
      Enum.map(out, fn char -> key[char] end)
      |> Enum.sort()
    end)
    |> Enum.reduce("", fn k, s -> s <> @lookup[k] end)
    |> String.to_integer()
  end
end
