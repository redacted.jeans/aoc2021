defmodule AoC2021.Day14 do
  # Setup, etc.
  def setup(file) do
    [tpl | rules] =
      File.read!(file)
      |> String.split(["\n", "->"], trim: true)

    tpl =
      String.to_charlist(tpl)
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.frequencies()

    rules =
      rules
      |> Enum.map(&String.trim/1)
      |> Enum.chunk_every(2)
      |> Enum.reduce(%{}, fn [pair, insert], rules ->
        Map.put(rules, String.to_charlist(pair), String.to_charlist(insert))
      end)
    
    {tpl, rules}
  end

  # returns a list of all the characters' frequencies
  # (note the character themselves are discarded, since we only care about min & max)
  defp insert(pairs, _, 0) do
    pairs
    |> Enum.reduce(%{}, fn {[p1, p2], num}, fqs ->
      fqs
      |> Map.update(p1, num, &(&1 + num))
      |> Map.update(p2, num, &(&1 + num))
    end)
    |> Enum.map(fn {_, n} -> div(n + 1, 2) end)
  end

  defp insert(pairs, rules, n) do
    pairs
    |> Enum.reduce(%{}, fn {[p1, p2] = pair, num}, product ->
      [insert] = rules[pair]

      product
      |> Map.update([p1, insert], num, &(&1 + num))
      |> Map.update([insert, p2], num, &(&1 + num))
    end)
    |> insert(rules, n - 1)
  end

  # Part 1
  def part1({tpl, rules}) do
    insert(tpl, rules, 10)
    |> Enum.min_max()
    |> then(fn {min, max} -> max - min end)
  end

  # Part 2
  def part2({tpl, rules}) do
    insert(tpl, rules, 40)
    |> Enum.min_max()
    |> then(fn {min, max} -> max - min end)
  end
end
