defmodule AoC2021.Test14 do
  use ExUnit.Case, async: true

  alias AoC2021.Day14

  # template: NNCB
  @tpl %{'NN' => 1, 'NC' => 1, 'CB' => 1}
  @rules %{
    'CH' => 'B',
    'HH' => 'N',
    'CB' => 'H',
    'NH' => 'C',
    'HB' => 'C',
    'HC' => 'B',
    'HN' => 'C',
    'NN' => 'C',
    'BH' => 'H',
    'NC' => 'B',
    'NB' => 'B',
    'BN' => 'B',
    'BB' => 'N',
    'BC' => 'B',
    'CC' => 'N',
    'CN' => 'C'
  }

  test "setup" do
    assert Day14.setup("#{__DIR__}/data/test.txt") === {@tpl, @rules}
  end

  test "part1" do
    assert Day14.part1({@tpl, @rules}) === 1_588
  end

  test "part2" do
    assert Day14.part2({@tpl, @rules}) === 2_188_189_693_529
  end
end
