defmodule AoC2021.Day21 do
  # map of %{values => frequencies} we get when adding up three dirac dice rolls
  @drolls %{3 => 1, 4 => 3, 5 => 6, 6 => 7, 7 => 6, 8 => 3, 9 => 1}

  # Setup, etc.
  def setup(file) do
    File.read!(file)
    |> String.split("\n", trim: true)
    |> Enum.map(fn str ->
      String.split(str, ":", trim: true)
      |> Enum.at(-1)
      |> String.trim()
      |> String.to_integer()
      # 0-index position to make life easier
      |> then(&%{:position => &1 - 1, :score => 0})
    end)
  end

  # Part 1
  def part1([p1, p2]), do: play(p1, p2)

  # FIXME: a detdie's rolls are a pure function of how many times it's been rolled before,
  #        which we already keep track of in play(_, _, _, n); that might be more efficient?
  # create a deterministic die (detdie) whose last roll was n (unrolled if none provided)
  defp detdie(), do: detdie(0)
  defp detdie(n), do: Stream.iterate(n + 1, &(rem(&1, 100) + 1))

  # roll die n times and return {updated die, sum of rolls}
  defp roll(die, n) do
    rolls = Enum.take(die, n)
    die = detdie(Enum.at(rolls, -1))
    {die, rolls |> Enum.sum()}
  end

  # init call: create a new unused detdie, rolled 0 times
  defp play(p1, p2), do: play(p1, p2, detdie(), 0)
  # because the last incremented player is always p2, no need to check both
  defp play(p1, p2, _, n) when p2.score >= 1000, do: p1.score * n

  defp play(p1, p2, die, n) do
    # roll the die
    {die, move} = roll(die, 3)
    # update p1 position & score (+1 to account for 0-indexed position)
    position = rem(p1.position + move, 10)
    p1 = %{:position => position, :score => p1.score + position + 1}
    # keep playing (swap p1 & p2 so that p2 plays the next round)
    play(p2, p1, die, n + 3)
  end

  # Part 2
  # game states: %{game state => number of times it occurs}, {p1 wins, p2 wins}
  #      where game state = {p1 position, p1 score, p2 position, p2 score}
  def part2([p1, p2]), do: sim(%{{p1, p2} => 1})

  defp sim(states), do: sim(states, [0, 0])
  defp sim(states, wins) when map_size(states) === 0, do: Enum.max(wins)
  # recursive case: we still have non-winning states to look at
  defp sim(states, [w1, w2]) do
    # FIXME: can we do this map and the later reduce (w1 = ...) in one comprehension?
    states =
      for {{p1, p2}, n} <- states,
          {move, rolls} <- @drolls,
          p1.score < 21 and p2.score < 21 do
        position = rem(p1.position + move, 10)
        p1 = %{:position => position, :score => p1.score + position + 1}
        {{p2, p1}, n * rolls}
      end
      |> List.flatten()
      |> Enum.reduce(%{}, fn {k, v}, map -> Map.update(map, k, v, &(&1 + v)) end)

    # check p1's score (in p2's slot since we flipped them all)
    w1 =
      Enum.reduce(states, w1, fn
        {{_, %{:score => score}}, n}, wins when score >= 21 -> wins + n
        _, wins -> wins
      end)

    sim(states, [w2, w1])
  end
end
