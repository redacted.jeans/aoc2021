defmodule AoC2021.Test21 do
  use ExUnit.Case, async: true

  alias AoC2021.Day21

  @data [
    %{:position => 4 - 1, :score => 0},
    %{:position => 8 - 1, :score => 0}
  ]

  test "setup" do
    assert Day21.setup("#{__DIR__}/data/test.txt") === @data
  end

  test "part1" do
    assert Day21.part1(@data) === 739_785
  end

  test "part2" do
    assert Day21.part2(@data) === 444_356_092_776_315
  end
end
